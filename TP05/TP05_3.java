public class TP05_3 {
    public static void main (String args []) {
        Scanner sc = new Scanner(System.in);
        //soal adalah variabel untuk menampung matrix soal Dek Depe
        int soal[][]=new int[2][2]; 
        int det = 0;
        det = determinant(puzzle);
        System.out.println(invertAble(det));
    }
    /**
   * Metode ini untuk mencari determinan dari matrix
   * @param matrix adalah array dua dimensi 
   * @return int Metode ini mengembalikan determinan dalam bentuk integer
   */
    public static int determinant(int[][] matrix) {
        //TODO : Lengkapi Method ini
        
    
    } 
    /**
   * Metode ini untuk menentukan apakah matrix dapat diinverse atau tidak
   * @param matrix adalah array dua dimensi 
   * @return boolean Metode ini mengembalikan true, jika matrix dapat diinverse, false jika matrix tidak dapat diinverse
   */
    public static boolean invertAble(int determinan) {
        //TODO : Lengkapi Method ini

    }

}
