import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteFile {
    private String fileName;

    public WriteFile(String fileName) {
        this.fileName = fileName;
    }

    public void combineFiles(ArrayList<String[]> dataCust, ArrayList<String[]> transaksi) throws IOException {
        FileWriter file = new FileWriter(this.fileName);
        file.write("Id,Name,Date,Amount\n");
        dataCust = sortData(dataCust);

        for (int i = 1; i < dataCust.size(); i++) {
            int id1 = Integer.parseInt(dataCust.get(i)[0]);
            for (int j = 1; j < transaksi.size(); j++) {
                int id2 = Integer.parseInt(transaksi.get(j)[0]);
                if (id1 == id2) {
                    String nama = dataCust.get(i)[1];
                    String tanggal = transaksi.get(j)[1];
                    String total = transaksi.get(j)[2];
                    String data = String.format("%d,%s,%s,%s \n", id1, nama, tanggal, total);
                    file.write(data);
                }
            }
        }
        file.close();
    }

    private ArrayList<String[]> sortData(ArrayList<String[]> dataCust) {
        // sort dataCust berdasarkan ID
        // [3,1,5,2] -> [1,3,5,2] -> [1,2,5,3] -> [1,2,3,5]
        for (int i = 1; i < dataCust.size() - 1; i++) {
            int id1 = Integer.parseInt(dataCust.get(i)[0]);
            for (int j = i+1; j < dataCust.size(); j++) {
                int id2 = Integer.parseInt(dataCust.get(j)[0]);
                if (id1 > id2) {
                    String[] temp = dataCust.get(i);
                    dataCust.remove(i);
                    dataCust.add(i, dataCust.get(j));
                    dataCust.remove(j);
                    dataCust.add(j, temp);
                }
            }
        }
        return dataCust;
    }

    public void addRecord(ArrayList<String> lookup, String[] data) throws NumberFormatException, IOException {
        int ID = Integer.parseInt(data[0]); // Kena exception kalau string ID bukan angka
        lookup.add(data[0]);
        FileWriter file = new FileWriter(this.fileName, true); // writing file with append mode
        String temp = String.format("%d,%s,%s,%s \n", ID, data[1], data[2], data[3]);
        file.write(temp); // append data at the end of the file
        file.close();
    }

    public void updateFile(String ID, String nama) throws IOException {
        // buat update file data customer
        FileWriter file = new FileWriter(this.fileName, true);
        String temp = String.format("%s,%s\n", ID, nama);
        file.write(temp);
        file.close();
    }

    public void updateFile(String ID, String tanggal, String total) throws IOException {
        // buat update file transaksi
        FileWriter file = new FileWriter(this.fileName, true);
        String temp = String.format("%s,%s,%s\n", ID, tanggal, total);
        file.write(temp);
        file.close();
    }
}
