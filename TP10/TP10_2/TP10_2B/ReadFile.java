import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile {
    private ArrayList<String> lookup; // list nomor ID customer yang terpakai
    private String namaFile;

    public ReadFile(String nama) {
        this.lookup = new ArrayList<>();
        this.namaFile = nama;
    }

    public ArrayList<String[]> putIntoList() throws FileNotFoundException {
        /* [[isi file 1], [isi file 2], ...]
        ["1", "hello"],
        ["2", "world"],
        ...
        */
        ArrayList<String[]> list = new ArrayList<>();
        File file = new File(this.namaFile);
        Scanner sc = new Scanner(file);

        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split(",");
            this.lookup.add(data[0]);
            list.add(data);
        }
        sc.close();
        return list;
    }

    public ArrayList<String> getLookUp() {
        return this.lookup;
    }
}
