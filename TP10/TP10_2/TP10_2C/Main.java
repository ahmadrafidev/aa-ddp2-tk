import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        /*
        1. combine file
        2. update lookUp (tambah ID baru)
        3. update file dataCust.csv (tambah ID dan nama baru)
        4. update file transaksi.csv (tambah ID, tanggal, dan total baru)
        5. add record di file tujuan
        */
        if (args.length != 3) {
            throw new IOException("You must provide 3 arguments");
        }
        
        try {
            Scanner sc = new Scanner(System.in);
    
            ReadFile file1 = new ReadFile(args[0]); // file pertama
            ReadFile file2 = new ReadFile(args[1]); // file kedua

            WriteFile file3 = new WriteFile(args[0]); // untuk update file pertama
            WriteFile file4 = new WriteFile(args[1]); // untuk update file kedua
            WriteFile file5 = new WriteFile(args[2]); // file tujuan
            System.out.printf("%s berhasil dibuat \n", args[2]);

            ArrayList<String[]> dataCustomer = file1.putIntoList();
            ArrayList<String[]> transaksi = file2.putIntoList();
            ArrayList<String> lookUp = file2.getLookUp();
            file5.combineFiles(dataCustomer, transaksi);
            
            while (true) {
                System.out.print("Apa yang ingin anda lakukan? : ");
                String[] perintah = sc.nextLine().split(" ");
    
                if (perintah[0].equals("exit")) {
                    break;
                }

                if (perintah[0].equals("tambah") && perintah[1].equals("record")) {
                    System.out.print("Masukkan id : ");
                    String ID = sc.nextLine();
                    int numID = Integer.parseInt(ID); // will throw exception if ID is not numeric
                    while (lookUp.contains(ID)) {
                        System.out.printf("Pelanggan dengan id %s sudah ada! \n", ID);
                        System.out.print("Masukkan id : ");
                        ID = sc.nextLine();
                        numID = Integer.parseInt(ID);
                    }

                    System.out.print("Masukkan nama : ");
                    String nama = sc.nextLine();
                    System.out.print("Masukkan total harga : ");
                    String total = sc.nextLine();
                    DateFormat today = new SimpleDateFormat("yyyy/MM/dd"); // format date
                    String tanggal = today.format(new Date());
    
                    String[] temp = {ID, nama, tanggal, total};
                    file3.updateFile(ID, nama);
                    file4.updateFile(ID, tanggal, total);
                    file5.addRecord(lookUp, temp);
                    System.out.printf("Record baru berhasil ditambahkan : %s,%s,%s,%s \n", ID, nama, tanggal, total);
                }
            }
            sc.close();
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException : Terdapat kesalahan format input!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
