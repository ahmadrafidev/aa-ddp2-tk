import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.junit.Test;

public class UnitTest {
    @Test
    public void testInputArgs() {
        // test amount of arguments given from input
        assertThrows(IOException.class, () -> {
            String[] input = {"new.csv", "dataCust.csv", "hello.csv", "world.csv"};
            if (input.length != 3) {
                throw new IOException();
            }
        });
        assertThrows(IOException.class, () -> {
            String[] input = {"idk.csv"};
            if (input.length != 3) {
                throw new IOException();
            }
        });
        assertThrows(IOException.class, () -> {
            String[] input = {"test.csv", "new.csv"};
            if (input.length != 3) {
                throw new IOException();
            }
        });
        assertThrows(IOException.class, () -> {
            String[] input = {"world.csv"};
            if (input.length != 3) {
                throw new IOException();
            }
        });
    }

    @Test
    public void testFileExistence() {
        // test source files' existences
        assertThrows(FileNotFoundException.class, () -> {
            File file = new File("dataCust.csv");
            Scanner sc = new Scanner(file); // may throw exception if file does not exist
            sc.close();
        });
        assertThrows(FileNotFoundException.class, () -> {
            File file = new File("transaksi.csv");
            Scanner sc = new Scanner(file);
            sc.close();
        });
        assertThrows(FileNotFoundException.class, () -> {
            File file = new File("hello.csv");
            Scanner sc = new Scanner(file);
            sc.close();
        });
    }

    @Test
    public void testID() {
        // test if given ID is numeric
        assertThrows(NumberFormatException.class, () -> {
            String ID = "a";
            int numID = Integer.parseInt(ID);
        });
        assertThrows(NumberFormatException.class, () -> {
            String ID = "S";
            int numID = Integer.parseInt(ID);
        });
        assertThrows(NumberFormatException.class, () -> {
            String ID = "aiueo";
            int numID = Integer.parseInt(ID);
        });
    }
}
