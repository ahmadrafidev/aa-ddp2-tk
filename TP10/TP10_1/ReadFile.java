import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile {
    private ArrayList<String[]> temp = new ArrayList<String[]>();
    private ArrayList<String> lookup = new ArrayList<String>();
    private String fileName;

    //Lakukan inisialisasi file read
    public ReadFile(String file){
        this.fileName = file;
    }

    //Memasukkan isi file kedalam sebuah arraylist of array. Anda dapat memodifikasi bentuk penyimpanan sesuai kebutuhan.
    public ArrayList<String[]> putIntoList() throws FileNotFoundException {
        File file = new File(this.fileName);
        Scanner input = new Scanner(file);

        while (input.hasNextLine()) {
            String[] data = input.nextLine().split(",");
            this.lookup.add(data[0]);
            temp.add(data);
        }
        input.close();
        return temp;
    }

    //Lookup adalah kumpulan id yang sudah dipakai oleh customer
    public ArrayList<String> getLookup(){
        return this.lookup;
    }
}
