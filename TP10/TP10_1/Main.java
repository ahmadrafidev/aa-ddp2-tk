import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        /*
        1. combine file
        2. update lookUp (tambah ID baru)
        3. update file dataCust.csv (tambah ID dan nama baru)
        4. update file transaksi.csv (tambah ID, tanggal, dan total baru)
        5. add record di file tujuan
        */

        if (args.length != 3) {
            throw new IOException("You must provide 3 arguments");
        }
        try {
            Scanner sc = new Scanner(System.in);

            ReadFile file = new ReadFile(args[0]);
            ArrayList<String[]> dataCust = file.putIntoList();

            ReadFile file2 = new ReadFile(args[1]);
            ArrayList<String[]>transaksi = file2.putIntoList();

            //Memulai proses penulisan file
            WriteFile file3;
            System.out.printf("%s berhasil dibuat \n", args[2]);
            //Header pada file baru
            String header = "Id,Name,Date,Amount\n";
            file3 = new WriteFile(args[2], header);
            file3.combineFiles(dataCust, transaksi);

            while (true){
                System.out.print("Apa yang ingin anda lakukan? : ");
                String[] perintah = sc.nextLine().split(" ");

                if (perintah[0].equals("exit")) {
                    break;
                }

                if (perintah[0].equals("tambah") && perintah[1].equals("record")) {
                    System.out.print("Masukkan id : ");
                    String ID = sc.nextLine();
                    int numID = Integer.parseInt(ID); // will throw exception if ID is not numeric

                    ArrayList<String> lookup = file2.getLookup();
                    while (lookup.contains(ID)) {
                        System.out.printf("Pelanggan dengan id %s sudah ada! \n", ID);
                        System.out.print("Masukkan id : ");
                        ID = sc.nextLine();
                        numID = Integer.parseInt(ID);
                    }

                    System.out.print("Masukkan nama : ");
                    String nama = sc.nextLine();
                    System.out.print("Masukkan total harga : ");
                    String total = sc.nextLine();
                    DateFormat today = new SimpleDateFormat("yyyy/MM/dd"); // format date
                    String tanggal = today.format(new Date());

                    String[] temp = {ID, nama, tanggal, total};
                    file3.addRecord(lookup, temp);

                    System.out.printf("Record baru berhasil ditambahkan : %s,%s,%s,%s \n", ID, nama, tanggal, total);
                }
            } sc.close();

        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException : Terdapat kesalahan format input!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
