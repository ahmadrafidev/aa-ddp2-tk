import java.util.Random;
import java.util.Scanner;

public class TP03_3 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Konstanta
        final int MAX_PENGUNJUNG = 5;
        final boolean STATUS_SALAH = false;
        final boolean STATUS_BENAR = true;

        // Variabel Umum
        Random randomizer = new Random();
        char specialChar = '1';
        String nama = "";
        int jumlahPengunjung = 0;
        boolean status = true;
        boolean exit = true;

        System.out.println("Selamat datang di Sistem Gerbang Kedai Kopi VoidMain!");

        // TODO: Kerjakan logika penentuan karakter spesial di sini

        specialChar = getSpecialChar();

        // Jika dicetak masih 1, maka program pengecekan karaktermu belum tepat :)
        System.out.println("Karakter spesial hari ini adalah: " + (char) (specialChar - 32));

        System.out.println("=====PELAYANAN PENGUNJUNG======");
        System.out.println("Maksimum pengunjung saat pandemi: " + MAX_PENGUNJUNG);
        System.out.println("Untuk keluar, ketik 'exit'");
        System.out.println("===============================");

        while ((jumlahPengunjung < MAX_PENGUNJUNG) && (exit == true)) {
            nama = get_nama_pengunjung();

            if (nama.equals("exit")) {
                exit = false;
            } else{
                status = tebakKataSandi(nama, specialChar);
                if (status)
                    jumlahPengunjung++;
            }
        }

        System.out.println("Jumlah pengunjung kedai kopi: " + jumlahPengunjung + " orang");
        System.out.println("Hari selesai!");
        sc.close();

    }

    // Metode untuk Mendapatkan karakter spesial
    public static char getSpecialChar() {
        System.out.print("Apa karakter spesial untuk hari ini? (Kosongkan untuk menggunakan karakter acak): ");
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        String tempStr = sc.nextLine().toLowerCase();
        char tempChar;

        while ((!alphabets.contains(tempStr))) {
            System.out.println("Karakter harus merupakan alfabet!");
            System.out.print("Apa karakter spesial untuk hari ini? (Kosongkan untuk menggunakan karakter acak): ");
            tempStr = sc.nextLine().toLowerCase();
        }

        if (tempStr.isBlank()) {
            tempChar = getRandom();
        } else {
            tempChar = tempStr.charAt(0);
        }

        return tempChar;
    }

    // Metode untuk mendapatkan nama pengunjung
    public static String get_nama_pengunjung() {
        System.out.print("Masukkan nama pengunjung: ");
        String nama = sc.nextLine();
        return nama;
    }

    // Metode untuk cek password
    public static int cekPassword(String password, char specialChar) {
        int cnt = 0;
        for (int i = 0; i < password.length(); i++) {
            if (password.charAt(i) == specialChar) {
                cnt++;
            }
        }
        return cnt;
    }

    // Metode untuk menebak kata sandi
    public static boolean tebakKataSandi(String nama, char specialChar) {
        int cnt = 0;
        int chance = 3;
        String password;
        while (chance > 0) {
            System.out.print("Apa kata sandi Kedai Kopi VoidMain? ");
            password = sc.nextLine().toLowerCase();

            cnt = cekPassword(password, specialChar);

            if (cnt < 3) {
                System.out.println("Kata sandi salah! Sisa percobaan: " + (--chance));
            } else {
                System.out.println(nama + " berhasil masuk!");// nama tergantung method mintaNamaPengunjung
                return true;
            }
            if (chance == 0) {
                System.out.println(nama + " gagal masuk!");
                return false;
            }
        }
        return false;
    }

    // method untuk mengenerate karakter huruf secara acak
    public static char getRandom() {
        Random randomizer = new Random();
        int randomASCII = randomizer.nextInt(123 - 97) + 97;
        return (char) randomASCII;
    }
}
