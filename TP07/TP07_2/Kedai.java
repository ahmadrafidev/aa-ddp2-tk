import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

public class Kedai {  // TODO: Modifikasi kelas Restoran sehingga Simulator.java bisa berjalan
    private Burhan burhan;
    private double kas;
    private double idxK;
    private Queue<Pelanggan> antrian = new LinkedList<>();
    private Map<String, Integer> menu, hasilPenjualan;
    private boolean buka;
    
    Kedai(double kas, double uangBurhan) {   
        this();
        this.kas = kas;
        this.burhan.setUang(uangBurhan);
    }

    Kedai(double kas) {
        this();
        this.kas = kas;
    }

    Kedai() {
        this.burhan = new Burhan(10000);
        this.kas = 0.0;
        this.idxK = 100;
        this.hasilPenjualan = new HashMap<>();
        this.buka = true;
    }

    public void parseMenu(String menu) throws FileNotFoundException {  // TODO: Implementasikan method untuk memasukkan menu dari file txt menjadi object!
        // Hint: cek Seeder.java :D
        Seeder.seed(menu);
        File fileMenu = new File(menu);
        Scanner reader = new Scanner(fileMenu);
        this.menu = new HashMap<>();

        while (reader.hasNextLine()) {
            String data = reader.nextLine();
            if (data.isBlank()) {
                break;
            }
            String[] tempData = data.split(" ");
            String tempName = Arrays.toString(Arrays.copyOfRange(tempData, 0, tempData.length - 1)); // menu's name
            Integer tempPrice = Integer.parseInt(tempData[tempData.length - 1]); // menu's price
            this.menu.put(tempName, tempPrice);
        }
        reader.close();
    }

    public void layaniPelanggan() {  // TODO: Implementasikan method untuk melayani semua pelanggan di antrian!
        System.out.println("0 Restoran sedang melayani pelanggan.");
        System.out.println("==============Daftar Menu==============");
        for (Map.Entry<String, Integer> menu : this.menu.entrySet()) {
            System.out.printf("%-14s %14d DDD \n", menu.getKey(), menu.getValue());
        }
        System.out.println("==============Daftar Menu==============");
        Scanner sc = new Scanner(System.in);

        for (Pelanggan p : this.antrian) {
            System.out.printf("Pelanggan %s memesan : \n", p.getNama());
            ArrayList<String[]> listPesanan = new ArrayList<>();

            while (sc.hasNextLine()) {
                String[] input = sc.nextLine().split(" "); // input menu
                String menu = Arrays.toString(Arrays.copyOfRange(input, 0, input.length - 1));
                if (isAvailable(menu)) {
                    listPesanan.add(input);
                    Integer total = Integer.parseInt(input[input.length-1]) * Integer.parseInt(input[input.length-2]); // total harga pesanan
                    this.hasilPenjualan.put(menu, total); // mendata menu pesanan dan harga  
                    this.setIdxK(this.idxK - Integer.parseInt(input[input.length-2])); // mengurangkan idxK    
                } else {
                    System.out.println("Pesanan tidak ada di daftar Menu, mohon coba lagi.");
                }
            }
            Bon bon = this.burhan.buatBon(listPesanan, this.idxK);
            if (this.idxK > 0) {
                p.bayar(this.burhan, bon);
                this.setKas(this.kas + bon.hitungHargaTotal());
                System.out.printf("Uang pelanggan %s cukup, bon lunas. \n", p.getNama());
            } else {
                System.out.println("Index kebersihan Kedai telah mencapai 0!");
                System.out.printf("Pelanggan %s mendapatkan pesanannya secara gratis! \n", p.getNama());
            }
        }
        System.out.println("Pesanan telah diberikan, antrian kosong.");
        sc.close();
    }

    private boolean isAvailable(String menu) {
        // method untuk mengecek ketersediaan menu
        for (Map.Entry<String, Integer> tempMenu : this.menu.entrySet()) {
            String temp = tempMenu.getKey().toLowerCase();
            if (temp.equals(menu.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public void reset() { // TODO: Implementasikan method untuk mereset kedai sesuai permintaan soal!
        this.antrian.clear();
        this.menu.clear();
        this.kas = 0;
        this.burhan.setUang(10000);
        this.idxK = 100;
    }

    public void tutupKedai() { // TODO: Implementasikan method untuk rekap total pendapatan secara keseluruhan!
        System.out.println("=======Rekap Pendapatan Akhir=======");
        System.out.printf("Total pendapatan terakhir : %.2f \n", this.kas);
        System.out.println("Menu yang terjual pada sesi ini :");
        for (Map.Entry<String, Integer> hasil : hasilPenjualan.entrySet()) {
            System.out.printf("%s %d \n", hasil.getKey(), hasil.getValue());
        }
        System.out.println("=======Rekap Pendapatan Akhir=======");
        this.buka = false;
    }

    public Burhan getBurhan() {
        return this.burhan;
    }

    public Queue<Pelanggan> getAntrian() {
        return this.antrian;
    }

    public String getDaftarAntrian() {
        return Arrays.toString(this.antrian.toArray());
    }

    public Pelanggan getPelangganTerdepan() {
        return this.antrian.poll();
    }

    public void tambahAntrianPelanggan(Pelanggan p){
        this.antrian.add(p);
    }

    public double getKas() {
        return this.kas;
    }

    public void setKas(double kas) {
        this.kas = kas;
    }

    public double getIdxK(){
        return this.idxK;
    }

    public void setIdxK(double idxK) {
        this.idxK = idxK;
    }

    @Override
    public String toString() {
        return "Kedai voidMain, dijaga solo oleh Kak Burhan. Total uang kas sekarang ="+Double.toString(kas);
    }
    
    public static void print(String s) {
        System.out.println(s);
    }
    public static void print(int i) {
        System.out.println(i);
    }
    public static void print(double i) {
        System.out.println(i);
    }

}