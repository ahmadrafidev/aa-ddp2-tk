public class Pelanggan {
    // TODO: Tambahkan field yang dibutuhkan disini
    private String nama;
    private double uang;
    public Pelanggan(String nama, double uang){   // TODO: Lengkapi constructornya
        this.nama = nama;
        this.uang = uang;
    }
    // TODO: Lengkapi method Getter dan Setternya
    /*
    Getter dan Setter
     */
    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public double getUang(){
        return this.uang;
    }

    public void setUang(double uang){
        this.uang = uang;
    }

    /**
     * Method untuk membayar pesanan sesuai harga di bon
     * @param burhan
     * @param bon
     */
    public void bayar(Burhan burhan, Bon bon){
        if (this.uang < bon.hitungHargaTotal()){
            this.mintaBantuan(burhan, bon);
            this.uang = 0;
        } else {
            this.uang = this.uang - bon.hitungHargaTotal();
        }
    }

    /**
     * Method untuk meminta bantuan kepada class Burhan apabila uang
     * pelanggan tidak mencukupi
     * @param burhan
     * @param bon
     */
    public void mintaBantuan(Burhan burhan, Bon bon){
        burhan.bantu(this, (bon.hitungHargaTotal() - this.uang));
    }
    
    public String toString() {
        return this.nama;
    }
}