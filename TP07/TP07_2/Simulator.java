

import java.util.Scanner;


public class Simulator {    // NOTE: Silakan gunakan template yang diberikan sesuai keinginan dan kepercayaan masing-masing.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String menu ;

        // Hardcoded Testcase, hasil tergantung interaksi pelanggan.
        Kedai voidMain = new Kedai(0.0, menu);
        Burhan burhan = voidMain.getBurhan();
        voidMain.tambahAntrianPelanggan(new Pelanggan("A", 20000.0));
        voidMain.tambahAntrianPelanggan(new Pelanggan("B", 20000.0));
        voidMain.layaniPelanggan();
        burhan.bersihkan(voidMain);
        burhan.setUang(100000.0);
        voidMain.tambahAntrianPelanggan(new Pelanggan("C", 50000.0));
        voidMain.tambahAntrianPelanggan(new Pelanggan("D", 50000.0));
        voidMain.layaniPelanggan();
        for (int i = 1; i < 4; i++) {
            voidMain.tambahAntrianPelanggan(new Pelanggan(Integer.toString(i), 50000.0));
        }
        voidMain.layaniPelanggan();
        voidMain.tutup();
        
        // ======================Batas Hardcoded Test Case==========================================

        // reset
        voidMain.reset();
        
        // ==============================User Input===============================================
        String perintah;
        do {
            System.out.println("Apa yang saya bisa bantu? ");
            perintah = scanner.nextLine();
            /**
             * Lengkapi switch case berikut supaya bisa menjalankan 
             * semua perintah yang diminta oleh soal!
             */
            switch (perintah.toUpperCase()) { 

                case "RESET":
                    
                    voidMain.reset() ;
                    
                    break;

                case "TAMBAH PELANGGAN " :
                
                    int jumlah_pelanggan ;
                    String pilihan ;
                    String nama ;
                    double uang ;
                    System.out.println("Masukkan jumlah pelanggan : ");
                    jumlah_pelanggan = scanner.nextInt() ;
                    System.out.println("Masukkan metode penambahan pelanggan, manual atau loop");
                    pilihan = scanner.nextLine() ;

                    if (pilihan.toLowerCase() == "loop"){

                      for (int i=0 ; i < jumlah_pelanggan ; i++){
                            
                        System.out.println("Masukkan nama pelanggan : ");
                        nama = scanner.nextLine() ;
                        
                        System.out.println("Masukkan uang pelanggan : ");
                        uang = scanner.nextInt() ;

                        voidMain.tambahAntrianPelanggan(new Pelanggan(nama, uang));
                      }
                    
                        break ;
                    }
                    else if (pilihan.toLowerCase() == "manual") {
                      
                        voidMain.tambahAntrianPelanggan(new Pelanggan("B", 20000.0));

                        break ;
                    }

                
                case "LIHAT ANTREAN" :

                    voidMain.getDaftarAntrian() ;

                    break ;
                
                case "LAYANI PELANGGAN" :

                    voidMain.layaniPelanggan();

                    break ;

                case "BERSIHKAN KEDAI" :

                    burhan.bersihkan(voidMain) ;

                    break ;

                case "TAMBAH UANG" :
                    
                    double bansos_tunai ;
                    System.out.println("Kak Burhan butuh uang. Mau nyumbang berapa?");
                    bansos_tunai = scanner.nextDouble() ;
                    burhan.setUang(bansos_tunai) ;

                    break ;
                
                case "SET UANG " :

                    double dompet_Kak_Burhan ;
                    System.out.println("Kak Burhan butuh uang. Mau nyumbang berapa?");
                    dompet_Kak_Burhan = scanner.nextDouble() ;
                    burhan.setUang(dompet_Kak_Burhan) ;

                    break ;

                case "GANTI MENU" :
                    
                    String menu_baru ;
                    System.out.println("Masukkan menu baru untuk menggantikan menu lama : ");
                    menu_baru = scanner.nextLine() ;
                    voidMain.parseMenu(menu_baru);

                    break ;

                case "TUTUP KEDAI" :
                    
                    voidMain.tutup();

                    break ;

                default :
                    System.out.println("Maaf, saya tidak mengerti permintaan Anda");
                    break;
                
            }

        } while (perintah.equalsIgnoreCase("Tutup Kedai"));
        voidMain.tutup();
    }
    public static void print(String s) {
        System.out.println(s);
    }
    public static void print(int i) {
        System.out.println(i);
    }
    public static void print(double i) {
        System.out.println(i);
    }
}
