import java.util.ArrayList;

public class Burhan {
    private String nama;
    private double uang;
    // TODO: Tambahkan field yang dibutuhkan disini
    // TODO: Lengkapi constructornya

    Burhan() {   
        this.nama = "Kak Burhan";
        this.uang = 10000;
    }

    Burhan(double uang) {
        this();
        this.uang = uang;
    }
    // TODO: Lengkapi method Getter dan Setternya

    public double getUang() {
        return this.uang;
    }

    public void setUang(double jumlah) {
        this.uang = jumlah;
    }
    
    public String toString() {
        return this.nama;
    }

    public void bantu(Pelanggan p, double jumlah) {
        // untuk mendonasikan uang kepada pelanggan p
        this.uang -= jumlah;
        p.setUang(p.getUang() + jumlah);
    }

    public void bersihkan(Kedai k) {
        // membersihkan kedai k dengan me-reset index kebersihannya
        k.reset();
    }

    public Bon buatBon(ArrayList<String[]> listPesanan, double idxK) {
        return new Bon(listPesanan, idxK);
    }
}
