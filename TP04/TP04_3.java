import java.util.Scanner;


public class TP04_3 {
    static String[] menus = new String[3];
    static double[] prices = new double[3];
    public static void main(String[] args) {
        mainMenu();
    }

    public static String termurahOrTermahal(String message, String[] menus, double[] prices) {
        /*
            Method untuk mencari menu termahal atau termurah sesuai pesan dari argumen method
        */ 
        String tempMenu = menus[0];
        double tempPrice = prices[0];

        for (int i = 1; i < prices.length; i++) {
            if ((message.equals("termahal") && tempPrice < prices[i]) || (message.equals("termurah") 
            && tempPrice > prices[i])) {
                tempMenu = menus[i];
                tempPrice = prices[i];
            }
        }
        return tempMenu;
    }

    public static String terfavorit(String menu1, String menu2, String menu3) {
        /*
            Method untuk mencari terfavorit dengan cara membandingkan
            setiap char
        */

        // Iteration berguna untuk mencari string terpendek yang akan dijadikan
        // Acuan untuk dibandingkan dengan cara looping satu per satu
        
        int iteration;

        if (menu1.length() <= menu2.length() && menu1.length() <= menu3.length()) {
            iteration = menu1.length();
        } else if(menu2.length() <= menu1.length() && menu2.length() <= menu3.length()) {
            iteration = menu2.length();
        } else {
            iteration = menu3.length();
        }

        char char1;
        char char2;
        char char3;
        String favorit = "";

        for (int i = 0; i < iteration; i++) {
            char1 = menu1.charAt(i);
            char2 = menu2.charAt(i);
            char3 = menu3.charAt(i);
            if (char1 < char2 && char1 < char3) {
                favorit = menu1;
                i = iteration;
            } else if (char2 < char1 && char2 < char3) {
                favorit = menu2;
                i = iteration;
            } else if (char3 < char1 && char3 < char2) {
                favorit = menu3;
                i = iteration;
            }
        }
        return favorit;
    }

    public static String terfavorit2(String[] menus){
        /*
            Method untuk menentukan menu terfavorit
        */
        int iteration;
        String tempMenu;
        String favorit = menus[0];

        for (int i = 1; i < menus.length; i++){
            if (favorit.length() > menus[i].length()){
                tempMenu = menus[i];
            } else{
                tempMenu = favorit;
            }
            iteration = tempMenu.length();

            char charMenu1;
            char charMenu2;
            for (int j = 0; j < iteration; j++){
                charMenu1 = favorit.charAt(j);
                charMenu2 = menus[i].charAt(j);
                if (charMenu1 > charMenu2){
                    favorit = menus[i];
                    j = iteration;
                } else if (charMenu1 < charMenu2){
                    j = iteration;
                }
            }
        }
        return favorit;
    }

    public static int splitWords(String menu){
        /* 
            Method untuk menghitung jumlah kata dalam satu menu
        */
        int count = 0;
        String[] words = menu.split(" ");
        for (int j = 0; j < words.length; j++){
            count++; 
        }
        return count;
    }

    
    public static int splitWords2(String menu)
    {

        int count = 0 ;
        int j = 0 ;
        String[] words = menu.split(" ") ;
        
        while (j < words.length)
        {
            j ++ ;
            count ++ ;
        }
        
        return count;
    
    }



    public static String tercepat(String[] menus) {
        /*
            Method untuk menghitung waktu tiap menu dalam detik
         */
        int time1 = 2 + splitWords2(menus[0]) * 600;
        int time2 = 5 + splitWords2(menus[1]) * 600;
        int time3 = 10 + splitWords2(menus[2]) * 600;

        if (time1 < time2 && time1 < time3) {
            return menus[0];
        } else if(time2 < time1 && time2 < time3) {
            return menus[1];
        } else {
            return menus[2];
        }
    }

    public static String tercepatVersi2() {
        String tempMenu = menus[0];
        int time1 = splitWords(menus[0]) * 600;
        int time2;

        for (int i = 1; i < prices.length; i++) {
            time2 = splitWords(menus[i]) * 600;
            if (time1 > time2) {
                time1 = time2;
                tempMenu = menus[i];
            }
        }
        return tempMenu;
    }

    public static String harusBeliOrTerbaik(String[] menu, double[] prices){

        /*
            Method untuk mencari Menu Terbaik
        */


        String BeliOrTerbaik = "";

        for(int i = 0; i < menu.length; i++){

            if ((menu[i] == terfavorit(menu[0], menu[1], menu[2])) && (menu[i] == tercepat(menu)))  {

                if (menu[i] == termurahOrTermahal("termahal", menu, prices)){
                    BeliOrTerbaik = menu[i] + " Menu Terbaik Hari Ini loh!";
                    return BeliOrTerbaik;
                }else if (menu[i] == termurahOrTermahal("termurah", menu, prices)){
                    BeliOrTerbaik = menu[i] + " Harus Banget Kamu Beli!";
                    return BeliOrTerbaik;
                }
            }

        }
        return BeliOrTerbaik;
    }

    public static void mainMenu(){
        
        Scanner sc = new Scanner(System.in);
       
        // Pengelolaan input menu
        
        System.out.println("==================================================================") ;
        System.out.println("Selamat Datang di Kedai Kopi VoidMain!");
        System.out.println("Menu Spesial Hari Ini : ");
        
        for (int i = 0 ; i < menus.length ; i++)
        {
         menus[i] = sc.nextLine();
        }
 
        prices[0] = 0.3 + menus[0].length();
        prices[1] = 0.5 + menus[1].length();
        prices[2] = 0.7 + menus[2].length();
 
        if (!harusBeliOrTerbaik(menus, prices).isEmpty()){
            System.out.println(harusBeliOrTerbaik(menus, prices));
        }
 
        System.out.println("Okay, sekarang mau menu yang kayak gimana nih ?");

        String rekomendasiMenu = "";

        do{

            rekomendasiMenu = sc.nextLine().toLowerCase();

            if (rekomendasiMenu.equals("termurah")) {
                System.out.println("Menu termurah hari ini : " + termurahOrTermahal(rekomendasiMenu, menus, prices));
            } else if (rekomendasiMenu.equals("termahal")) {
                System.out.println("Menu termahal hari ini : " + termurahOrTermahal(rekomendasiMenu, menus, prices));
            } else if (rekomendasiMenu.equals("terfavorit")) {
                System.out.println("Menu terfavorit hari ini : " + terfavorit(menus[0], menus[1], menus[2]));
            } else if (rekomendasiMenu.equals("tercepat")) {
                System.out.println("Menu tercepat hari ini : " + tercepat(menus));
            }

        } while(!rekomendasiMenu.equals("exit"));
    
        System.out.println("Jangan bosan mampir di kedai VoidMain ya, Kak!");
        sc.close();
    }

}


