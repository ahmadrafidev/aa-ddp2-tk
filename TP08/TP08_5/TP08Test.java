import static org.junit.Assert.*; 
import junit.framework.Assert;
import java.util.Scanner;
import org.junit.Test;


public class TP08Test {

  
    @Test 
    public void TestManusia(){

      System.out.println("-----------------------------------------------------------");
       
      System.out.println(" Kita akan menguji program Manusia");

      Manusia hooman = new Manusia() ;

      int jumlah_bug ;

      Assert.assertEquals(Manusia, actual);
      
      if (hooman.getNama("Imam") != "Imam"){

        
        jumlah_bug += 1 ;

      }

      else {
        
        System.out.println(" Tidak ada yang salah dari atribut get.Nama() pada program Manusia");

        if (hooman.berjalan() != "Halo, aku sedang berjalan"){

          Assert.fail(" Seharusnya program menampilkan 'Halo, aku sedang berjalan' ");
          jumlah_bug += 1 ;
          
        }

        else {

          System.out.println(" Tidak ada yang salah dari metode 'berjalan' pada program Manusia");

          String nama = "Imam" ;

          if (hooman.bicara() != "Halo, namaku " + nama){
             
            Assert.fail(" Seharusnya program menampilkan 'Halo, namaku Imam' ");
            jumlah_bug += 1 ;

          }

          else {

            System.out.println(" Keren, tidak ada bug yang terdeteksi di program ini !");
          }

        }

      }

      System.out.println("Pengujian program Manusia selesai!");

      System.out.println("Total bug yang terdeteksi : " + jumlah_bug);

      System.out.println("-----------------------------------------------------------");

    }


    @Test 
    public void TestPegawai(){

      System.out.println("-----------------------------------------------------------");

      System.out.println(" Kita akan menguji program Pegawai");

      Pegawai hooman = new Pegawai() ;

      int jumlah_bug ;
      
      if (hooman.getlevel("Noob") != "Noob"){

        Assert.fail(" Seharusnya level keahlian pegawai tersebut itu adalah 'Noob' !");
        jumlah_bug += 1 ;

      }

      else {
        
        System.out.println(" Tidak ada yang salah dari atribut get.level() pada program Pegawai");

        if (hooman.bicara() != "Selamat datang di Kedai"){

          Assert.fail(" Seharusnya program menampilkan 'Selamat datang di Kedai' ");
          jumlah_bug += 1 ;
          
        }

        else {

          System.out.println(" Tidak ada yang salah dari metode 'bicara' pada program Pegawai");

          String nama = "Imam" ;

          if (hooman.berjalan() != "Aku sedang berjalan!" || nama + " sedang berjalan!"){
             
            Assert.fail(" 'Imam sedang berjalan!' adalah output yang seharusnya!");
            jumlah_bug += 1 ;

          }

          else {

            System.out.println(" Keren, tidak ada bug yang terdeteksi di program ini !");
          }

        }

      }

      System.out.println("Pengujian program Pegawai selesai!");

      System.out.println("Total bug yang terdeteksi : " + jumlah_bug);

      System.out.println("-----------------------------------------------------------");

    }


    @Test 
    public void TestPelanggan(){

      System.out.println("-----------------------------------------------------------");

      System.out.println(" Kita akan menguji program Pelanggan");
      
      Pelanggan hooman = new Pelanggan() ;

      int jumlah_bug ;
      

        String nama = "Imam" ;
        int langkah = 100 ;
        
        System.out.println(" Tidak ada yang salah dari atribut getnoPelanggan() pada program Pelanggan");

        if (hooman.berjalan() != "Aku sedang berjalan!" || nama + " sedang berjalan!" || "Aku sedang berjalan " + langkah + " langkah!" ){

          Assert.fail(" Seharusnya program menampilkan 'Aku sedang berjalan!' atau 'Imam sedang berjalan!' atau 'Aku sedang berjalan 100 langkah!");
          jumlah_bug += 1 ;
          
        }

        else {

          System.out.println(" Tidak ada yang salah dari metode 'berjalan' pada program Pelanggan");

          if (hooman.bicara() != "Halo! Aku ingin pergi ke Kedai!"){
             
            Assert.fail(" Seharusnya program menampilkan 'Halo! Aku ingin pergi ke Kedai!' ");
            jumlah_bug += 1 ;

          }

          else {
            
            System.out.println(" Tidak ada yang salah dari metode 'bicara' pada program Pelanggan");

            if (hooman.beli() != "Aku akan membeli minuman!"){

              Assert.fail(" Seharusnya program menampilkan 'Aku akan membeli minuman!' ");
              jumlah_bug += 1 ;
            }

            else
               
             System.out.println(" Keren, tidak ada bug yang terdeteksi di program ini !");

          }

          
        }

      

      System.out.println("Pengujian program Pelanggan selesai!");

      System.out.println("Total bug yang terdeteksi : " + jumlah_bug);


      System.out.println("-----------------------------------------------------------");

    }

  


}