import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, Pegawai> daftarPegawai = new HashMap<>();
        Map<String, Pelanggan> daftarPelanggan = new HashMap<>();

        System.out.print("Ada berapa manusia : ");
        int total = sc.nextInt();

        for (int i = 0; i < total; i++) {
            System.out.print("Siapa Anda : ");
            String type = sc.next().toLowerCase();

            if (type.equals("pegawai")) {
                System.out.print("Masukkan data-data : ");
                sc.nextLine();
                String[] data = sc.nextLine().split(" ");
                daftarPegawai.put(data[0], new Pegawai(data[0], Double.parseDouble(data[1]), Integer.parseInt(data[2]), data[3]));
            } else {
                System.out.print("Masukkan data-data : ");
                sc.nextLine();
                String[] data = sc.nextLine().split(" ");
                daftarPelanggan.put(data[0], new Pelanggan(data[0], Double.parseDouble(data[1]), Integer.parseInt(data[2])));
            }
        }
        System.out.println("-------------------");
        System.out.println("Data-data berhasil dimasukan! Terimakasih!");

        while (true) {
            System.out.print("Perintah : ");
            String[] perintah = sc.nextLine().split(" ");
            int jumlahLangkah;

            if (perintah[0].equals("selesai")) {
                System.out.println("Sampai jumpa!");
                break;
            }
            if (daftarPegawai.containsKey(perintah[0])) {
                Pegawai temp = daftarPegawai.get(perintah[0]);
                switch (perintah[1]) {
                    case ("bicara") -> { temp.bicara(); }
                    case ("berjalan") -> { temp.berjalan(); }
                    case ("bandingkanDengan") -> {
                        if (daftarPegawai.containsKey(perintah[2])){
                            Pegawai temp2 = daftarPegawai.get(perintah[2]);
                            Manusia.bandingkanUang(temp, temp2);
                        } else if (daftarPelanggan.containsKey(perintah[2])){
                            Pelanggan temp2 = daftarPelanggan.get(perintah[2]);
                            Manusia.bandingkanUang(temp, temp2);
                        } else{
                            System.out.printf("Tidak ada yang bernama %s! \n", perintah[2]);
                        }
                    }
                    default -> { System.out.println("Perintah tidak dapat dilakukan"); }
                }
            } else if (daftarPelanggan.containsKey(perintah[0])) {
                Pelanggan temp = daftarPelanggan.get(perintah[0]);
                switch (perintah[1]) {
                    case ("bicara") -> { temp.bicara(); }
                    case ("berjalan") -> {
                        if (perintah.length == 3){
                            jumlahLangkah = Integer.parseInt(perintah[2]);
                            temp.berjalan(jumlahLangkah);
                        } else {
                            temp.berjalan();
                        }
                    }
                    case ("bandingkanDengan") -> {
                        if (daftarPegawai.containsKey(perintah[2])){
                            Pegawai temp2 = daftarPegawai.get(perintah[2]);
                            Manusia.bandingkanUang(temp, temp2);
                        } else if (daftarPelanggan.containsKey(perintah[2])){
                            Pelanggan temp2 = daftarPelanggan.get(perintah[2]);
                            Manusia.bandingkanUang(temp, temp2);
                        } else{
                            System.out.printf("Tidak ada yang bernama %s! \n", perintah[2]);
                        }
                    }
                    case ("beli") -> { temp.beli(); }
                    default -> { System.out.println("Perintah tidak dapat dilakukan"); }
                }
            } else {
                System.out.printf("Tidak ada yang bernama %s! \n", perintah[0]);
            }
        }
        sc.close();
    }
}
