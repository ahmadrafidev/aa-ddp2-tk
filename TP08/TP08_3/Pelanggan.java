public class Pelanggan extends Manusia {
    private int noPelanggan;
    private static int nomor = 1;

    Pelanggan(String nama, double uang, int umur) {
        super(nama, uang, umur);
        this.noPelanggan = nomor++;
    }

    @Override
    public void bicara() {
        System.out.println("Halo! Aku ingin pergi ke Kedai!");
    }

    @Override
    public void berjalan() {
        System.out.println("Aku sedang berjalan!");
    }

    //Overload method berjalan
    public void berjalan(int langkah){
        System.out.println("Aku sedang berjalan " + langkah + " langkah!");
    }

    public void beli() {
        System.out.println("Aku akan membeli minuman!");
    }

}
