public class Manusia {
    private String nama;
    private double uang;
    private int umur;

    Manusia(String nama, double uang, int umur) {
        this.nama = nama;
        this.uang = uang;
        this.umur = umur;
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void bicara() {
        System.out.printf("Halo, namaku %s \n", this.nama);
    }

    public void berjalan() {
        System.out.println("Halo, aku sedang berjalan");
    }

    public static void bandingkanUang(Manusia x, Manusia y) {
        if(x.uang == y.uang){
            System.out.println("Jumlah uang kedua manusia sama, yaitu " + x.uang);
        }else if(x.uang <= y.uang){
            System.out.println(x.nama + " (" + x.uang + ") " + "memiliki lebih sedikit uang dari " + y.nama + " (" + y.uang + ") ");
        }else if(x.uang >= y.uang){
            System.out.println(x.nama + " (" + x.uang + ") " + "memiliki lebih banyak uang dari " + y.nama + " (" + y.uang + ") ");         
        }
        
    }
}


