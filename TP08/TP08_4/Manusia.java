public class Manusia {
    private String nama;
    protected double uang;
    private int umur;

    Manusia(String nama, double uang, int umur) {
        this.nama = nama;
        this.uang = uang;
        this.umur = umur;
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur(){
        return this.umur;
    }

    public void setUmur(int umur) {
        this.umur = umur ;
    }

    public void bicara() {
        System.out.printf("Halo, namaku %s \n", this.nama);
    }

    public void berjalan() {
        System.out.println("Halo, aku sedang berjalan");
    }

    public String toString(){
        return this.nama + " " + this.uang + " " + this.umur;
    }
}
