public class Pegawai extends Manusia {
    private String levelKeahlian;

    Pegawai(String nama, double uang, int umur, String level) {
        super(nama, uang, umur);
        this.levelKeahlian = level;
    }

    @Override
    public void bicara() {
        System.out.println("Selamat datang di Kedai!");
    }

    @Override
    public void berjalan() {
        System.out.println(super.getNama() + " sedang berjalan!");
    }

    @Override
    public String toString(){
      
        return super.getNama() + " " + "pegawai" + "umur " + super.getUmur() + " " + levelKeahlian + " " + "jumlah uang" + super.uang;
    }
}
