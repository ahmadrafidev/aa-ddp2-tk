public class Manusia {
    private String nama;
    private double uang;
    private int umur;

    Manusia(String nama, double uang, int umur) {
        this.nama = nama;
        this.uang = uang;
        this.umur = umur;
    }

    public void bicara() {
        System.out.printf("Halo, namaku %s \n", this.nama);
    }

    public void berjalan() {
        System.out.println("Halo, aku sedang berjalan");
    }
}